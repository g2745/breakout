GDPC                                                                                <   res://.import/ball.png-ced949aa27aa6415675b42ae2cf4b2b4.stex�#      H       �Z�1=�z%֭�I@   res://.import/brick.png-b95ec73c70364c1639d7f6bb53031bf8.stex   �&      P       7(hl�\��{pI<   res://.import/icon.png-487276ed1e3a0c39cad0279d744ee560.stex/      �      ���W}p	#�Nex�1@   res://.import/paddle.png-44fd2b6a89df7e66bb7be62b6c823d20.stex  �)      F       B��?Y"��C �2:e�D   res://.import/play1_screen.png-459948682aa57fadb7195ffa863c0b1a.stex�:      p      6V��j0����n�lD   res://.import/play_screen.png-f36ca6b9c6993300740fad28a0118d78.stex �V      �      �,\f�G����zoa   res://Mini Scenes/Ball.tscn p      ]      /ue�%�zo-19ldn    res://Mini Scenes/BrickOne.tscn �      �      �RzNYmy8�b�`$��   res://Scenes/LevelOne.tscn  �      �      ��3R,3>V:\�W%�$   res://Scenes/Scripts/Ball.gd.remap  �i      .       ��ڮ�4�!>`m�!�1     res://Scenes/Scripts/Ball.gdc   P      �      ��ܷn)h(DOT�&2$   res://Scenes/Scripts/Paddle.gd.remap�i      0       �م�Q��O�Xa    res://Scenes/Scripts/Paddle.gdc       �      �!������
�K�y$   res://Scenes/Scripts/World.gd.remap �i      /       ɀ��L�M�ԗaݺ�    res://Scenes/Scripts/World.gdc  �"            �P�f�SD��t��    res://Sprites/ball.png.import   @$      �      ��O(��.���Z    res://Sprites/brick.png.import  @'      �      �Y�z��Óq����2    res://Sprites/paddle.png.import @*      �      �2�����ӒQ "��   res://default_env.tres  �,            т��	`�Lyb�Pw�v   res://icon.png  j      �      �~dg`!����I�҃   res://icon.png.import    8      �      ��fe��6�B��^ U�,   res://images/readme/play1_screen.png.import  T      �      �Iv͠�x����a+�,   res://images/readme/play_screen.png.import  �f      �      ����r҉����͊�   res://project.binary�w      X      ���K�P�Q=��;�y        [gd_scene load_steps=4 format=2]

[ext_resource path="res://Scenes/Scripts/Ball.gd" type="Script" id=1]
[ext_resource path="res://Sprites/ball.png" type="Texture" id=2]

[sub_resource type="RectangleShape2D" id=1]

custom_solver_bias = 0.0
extents = Vector2( 4, 4 )

[node name="Ball" type="RigidBody2D"]

input_pickable = false
collision_layer = 1
collision_mask = 1
mode = 2
mass = 1.0
friction = 0.0
bounce = 1.0
gravity_scale = 0.0
custom_integrator = false
continuous_cd = 0
contacts_reported = 1
contact_monitor = true
sleeping = false
can_sleep = true
linear_velocity = Vector2( 200, -200 )
linear_damp = 0.0
angular_velocity = 0.0
angular_damp = 0.0
script = ExtResource( 1 )
_sections_unfolded = [ "Angular", "Linear", "Transform" ]
__meta__ = {
"_edit_group_": true
}
speedup = 7

[node name="Sprite" type="Sprite" parent="." index="0"]

scale = Vector2( 4, 4 )
texture = ExtResource( 2 )
_sections_unfolded = [ "Transform" ]

[node name="Collision" type="CollisionShape2D" parent="." index="1"]

shape = SubResource( 1 )
_sections_unfolded = [ "Material", "Pause", "Transform", "Visibility", "Z Index" ]


   [gd_scene load_steps=3 format=2]

[ext_resource path="res://Sprites/brick.png" type="Texture" id=1]

[sub_resource type="RectangleShape2D" id=1]

custom_solver_bias = 0.0
extents = Vector2( 30, 14 )

[node name="Brick" type="StaticBody2D" groups=[
"Bricks",
]]

input_pickable = false
collision_layer = 1
collision_mask = 1
constant_linear_velocity = Vector2( 0, 0 )
constant_angular_velocity = 0.0
friction = 1.0
bounce = 0.0
_sections_unfolded = [ "Transform" ]
__meta__ = {
"_edit_group_": true
}

[node name="Sprite" type="Sprite" parent="." index="0"]

scale = Vector2( 2, 2 )
texture = ExtResource( 1 )
_sections_unfolded = [ "Transform" ]

[node name="CollisionShape2D" type="CollisionShape2D" parent="." index="1"]

shape = SubResource( 1 )


 [gd_scene load_steps=6 format=2]

[ext_resource path="res://Scenes/Scripts/World.gd" type="Script" id=1]
[ext_resource path="res://Scenes/Scripts/Paddle.gd" type="Script" id=2]
[ext_resource path="res://Sprites/paddle.png" type="Texture" id=3]
[ext_resource path="res://Mini Scenes/BrickOne.tscn" type="PackedScene" id=4]

[sub_resource type="RectangleShape2D" id=1]
extents = Vector2( 40, 4 )

[node name="World" type="Node2D"]
script = ExtResource( 1 )

[node name="Paddle" type="KinematicBody2D" parent="."]
position = Vector2( 324.911, 324.176 )
script = ExtResource( 2 )

[node name="Sprite" type="Sprite" parent="Paddle"]
scale = Vector2( 2, 2 )
texture = ExtResource( 3 )
__meta__ = {
"_edit_lock_": true
}

[node name="Collision" type="CollisionShape2D" parent="Paddle"]
shape = SubResource( 1 )

[node name="Anker" type="Position2D" parent="Paddle"]
position = Vector2( 0, 32 )

[node name="Walls" type="StaticBody2D" parent="."]

[node name="Collision" type="CollisionPolygon2D" parent="Walls"]
polygon = PoolVector2Array( 0.205856, 360.488, -20.2278, 360.488, -20.423, -19.3658, 658.92, -19.3658, 658.92, 362.101, 639.68, 360.774, 640.344, 1.20024, 0.143021, -0.126595 )

[node name="Bricks" type="Node2D" parent="."]
__meta__ = {
"_edit_group_": true
}

[node name="Brick" parent="Bricks" instance=ExtResource( 4 )]
position = Vector2( 85.5813, 51.0834 )

[node name="Brick2" parent="Bricks" instance=ExtResource( 4 )]
position = Vector2( 126.05, 84.2544 )

[node name="Brick3" parent="Bricks" instance=ExtResource( 4 )]
position = Vector2( 155.24, 49.7565 )

[node name="Brick4" parent="Bricks" instance=ExtResource( 4 )]
position = Vector2( 196.372, 84.2544 )

[node name="Brick5" parent="Bricks" instance=ExtResource( 4 )]
position = Vector2( 222.909, 50.42 )

[node name="Brick6" parent="Bricks" instance=ExtResource( 4 )]
position = Vector2( 264.705, 83.591 )

[node name="Brick7" parent="Bricks" instance=ExtResource( 4 )]
position = Vector2( 290.578, 50.42 )

[node name="Brick8" parent="Bricks" instance=ExtResource( 4 )]
position = Vector2( 333.037, 83.591 )

[node name="Brick9" parent="Bricks" instance=ExtResource( 4 )]
position = Vector2( 357.584, 51.0834 )

[node name="Brick10" parent="Bricks" instance=ExtResource( 4 )]
position = Vector2( 400.043, 84.2544 )

[node name="Brick11" parent="Bricks" instance=ExtResource( 4 )]
position = Vector2( 425.916, 52.4102 )

[node name="Brick12" parent="Bricks" instance=ExtResource( 4 )]
position = Vector2( 468.375, 84.2544 )

[node name="Brick13" parent="Bricks" instance=ExtResource( 4 )]
position = Vector2( 495.575, 53.0736 )

[node name="Brick14" parent="Bricks" instance=ExtResource( 4 )]
position = Vector2( 536.707, 84.9178 )

[node name="Brick15" parent="Bricks" instance=ExtResource( 4 )]
position = Vector2( 562.581, 52.4102 )

[node name="Score" type="Label" parent="."]
margin_left = 12.0
margin_top = 13.0
margin_right = 86.0
margin_bottom = 27.0
text = "Score: 0"
            GDSC            �      ��������τ�   ������ƶ   �������򶶶�   �����϶�   ������������������Ŷ   ���������������Ŷ���   ����׶��   �����Ŷ�   �������������������Ŷ���   ���϶���   ����������ƶ   �������Ӷ���   ����Ӷ��   ���������Ӷ�   �������Ӷ���   ����Ҷ��   ������������������϶   �����޶�   ��������ض��   �������ض���   ������������������ض   �������϶���   ���������Ҷ�   ������������������϶   ϶��   ����������������¶��   ��Ҷ         �              Bricks        /root/World             Paddle        Anker                      
                                 	      
         &      '      (      /      0      6      ?      H      N      O      Y      d      t      �      �      �      �      �      �      �      3YY8;�  SY:�  �  SYY0�  PQV�  �  �  �  P�  QYY0�  P�  QV�  �  �  ;�  �  PQ�  �  )�	  �  V�  &�	  T�
  P�  QV�  �  P�  QT�  �  �  �	  T�  PQ�  �  &�	  T�  PQ�  V�  ;�  �  PQT�  PQ�  ;�  �  �	  T�  P�  QT�  PQ�  ;�  �  T�  PQ�4  P�  �  R�  Q�  �  �  P�  Q�  �?  P�>  P�  �  QQ�  �  �  &�  T�  �  PQT�  T�  V�  �  PQ`    GDSC            �      ������������τ�   ���������Ӷ�   �����϶�   ������������������Ŷ   ����������������¶��   ���������������Ŷ���   ����׶��   ϶��   �������ض���   ������ζ   �����������¶���   �����������������ض�   ζ��   �����������ض���   �����¶�   ����¶��   ��������������������ض��   ���������Ҷ�   ���ڶ���   �������Ӷ���   �������Ӷ���   �������¶���   ��������Ҷ��      res://Mini Scenes/Ball.tscn                                                                        	      
         &      '      (      /      <      =      G      H      O      [      d      e      s      �      3YY:�  ?PQYY0�  PQV�  �  �  �  P�  Q�  �  P�  QYY0�  P�  QV�  �  �  ;�  �  T�  �  ;�	  �
  PQT�  PQT�  �  �  �  P�  P�	  R�  QQ�  Y0�  P�  QV�  &�  4�  �  T�  PQV�  ;�  �  T�  PQ�  �  �  T�  P�  �  P�  R�  QQ�  �  PQT�  PQT�  P�  QY`GDSC            &      ���ӄ�   ����Ӷ��   ��������Ӷ��   ����Ӷ��   �������Ӷ���   �������¶���             Score         Score:                     
                        3YY;�  9�  YY0�  P�  QV�  �  �  �  �  P�  QT�  P�  �>  P�  QQ`    GDST               ,   WEBPRIFF    WEBPVP8L   /� 0��?�������        [remap]

importer="texture"
type="StreamTexture"
path="res://.import/ball.png-ced949aa27aa6415675b42ae2cf4b2b4.stex"
metadata={
"vram_texture": false
}

[deps]

source_file="res://Sprites/ball.png"
dest_files=[ "res://.import/ball.png-ced949aa27aa6415675b42ae2cf4b2b4.stex" ]

[params]

compress/mode=0
compress/lossy_quality=0.7
compress/hdr_mode=0
compress/bptc_ldr=0
compress/normal_map=0
flags/repeat=0
flags/filter=true
flags/mipmaps=false
flags/anisotropic=false
flags/srgb=2
process/fix_alpha_border=true
process/premult_alpha=false
process/HDR_as_SRGB=false
process/invert_color=false
process/normal_map_invert_y=false
stream=false
size_limit=0
detect_3d=true
svg/scale=1.0
      GDST                4   WEBPRIFF(   WEBPVP8L   /��?M�#!z�9��@�| [remap]

importer="texture"
type="StreamTexture"
path="res://.import/brick.png-b95ec73c70364c1639d7f6bb53031bf8.stex"
metadata={
"vram_texture": false
}

[deps]

source_file="res://Sprites/brick.png"
dest_files=[ "res://.import/brick.png-b95ec73c70364c1639d7f6bb53031bf8.stex" ]

[params]

compress/mode=0
compress/lossy_quality=0.7
compress/hdr_mode=0
compress/bptc_ldr=0
compress/normal_map=0
flags/repeat=0
flags/filter=true
flags/mipmaps=false
flags/anisotropic=false
flags/srgb=2
process/fix_alpha_border=true
process/premult_alpha=false
process/HDR_as_SRGB=false
process/invert_color=false
process/normal_map_invert_y=false
stream=false
size_limit=0
detect_3d=true
svg/scale=1.0
   GDST(               *   WEBPRIFF   WEBPVP8L   /'�  �֦������            [remap]

importer="texture"
type="StreamTexture"
path="res://.import/paddle.png-44fd2b6a89df7e66bb7be62b6c823d20.stex"
metadata={
"vram_texture": false
}

[deps]

source_file="res://Sprites/paddle.png"
dest_files=[ "res://.import/paddle.png-44fd2b6a89df7e66bb7be62b6c823d20.stex" ]

[params]

compress/mode=0
compress/lossy_quality=0.7
compress/hdr_mode=0
compress/bptc_ldr=0
compress/normal_map=0
flags/repeat=0
flags/filter=true
flags/mipmaps=false
flags/anisotropic=false
flags/srgb=2
process/fix_alpha_border=true
process/premult_alpha=false
process/HDR_as_SRGB=false
process/invert_color=false
process/normal_map_invert_y=false
stream=false
size_limit=0
detect_3d=true
svg/scale=1.0
[gd_resource type="Environment" load_steps=2 format=2]

[sub_resource type="ProceduralSky" id=1]
radiance_size = 4
sky_top_color = Color( 0.0470588, 0.454902, 0.976471, 1 )
sky_horizon_color = Color( 0.556863, 0.823529, 0.909804, 1 )
sky_curve = 0.25
ground_bottom_color = Color( 0.101961, 0.145098, 0.188235, 1 )
ground_horizon_color = Color( 0.482353, 0.788235, 0.952941, 1 )
ground_curve = 0.01
sun_energy = 16.0

[resource]
background_mode = 2
background_sky = SubResource( 1 )
fog_height_min = 0.0
fog_height_max = 100.0
ssao_quality = 0
 GDST@   @            �  WEBPRIFF�  WEBPVP8L�  /?��m�׶����T��k��G��p��Lǁ�K
3F.3�l����Y������4��m7�n����,Y2��"Ë�=D@3:�m[Mc���Ʈ���Lɬ1=��6�� /�n�V!�n�
��Yn�8����{������B&2��̓K�a��K��iM���2a8���9RX�k� ��L
0/���p(�k$��1_R[�ٵp�H���8Iص�Şԗc�8�5�HX#'��̡E
E&š�5f �5r�Fra�Kg�ٴ	�����{��5f3^
E&�f<v9�j����Z�ɮ��aN����I3����:pg����̝Y��r ����xۙhD D��c���XL����xF�3E˒ H���[M����k	���H��A��]S(�l+" �e��dd��`wT�T4�o.o'��|qԥ:ʂ��*u���\�h����6�;*ƁQJ��,f.oG��o$rң���,'=�#�ŝ�%E�(72���?[��9h3�يT<!�Ff%? �����v�����o��?��?O?��'��77�O�h�D�_��\ގ*=�C���H�l|���[��vF�����E���lP{z�  �U�[�P���N��J�Ot�/��N%�:U��B�j�
@��c�R����ֿ����I�8s�Ck��$i	�t��v��L7-"��t��g '}���I�_���}��+�D��%��>9w������t$t?9���>9���`b�\�E���9ɂmx�+�Z��Mi���v�R��\�(�y���8ɗ����Ν���������ɐ�s��\C��.ogh�@ �kZ�������s�����r�C@�叱"/P��iI�h�98�R�?3�ώ��Xs�F��%�v2���c���s2���w%�����?����J
�j��J[�|�Uk,��v�M��݅����h�m���h��?H��3G辛3��#��?��������Ҵ��@ �����t�R�<��VC�8�cE�(��@ u�7���8x#���$@ �(�R>Vd���f�[�pHU���it��"p� I��/�f�P��*� n�?�k��B@��Ry �x�{� ��=Q��VQ�����+���o
@�6VrC� �0@@��̀�eA |��U)����g��?X�"�D l�#.�����)%]���L��KB�Tщ�VJ�"� [ ������T_�-j" ��%�;]��o���w���w�+K�Z�՝��饊�*����U�J4��g�Qw(����@7���;w���.�~.��:����˟�{�g%��������˟��vkf�Y?���(���K�yo�P�o2��]�Tn�Z��*�W*�UNi��dV�ɼ�v,��Pj����U_����HT6�9��yΩ#��5�����LyA[P:I��]������Fpdl������V�L6�|������9v��ڐH�X� �����g��NV� �5
�"�Q����E��x�x��X�k�OЉ���2Gz��rN��^�Y9T�5<x�Ez��ˇ�ʡ�WgN�������V���:|a ��������jZ�89��h"����ک��d�������A��o2k�ҼmLd�^s'�i%����V�� ���di�3(K���1fԹ'��������B�:ޭ��~2��̀X�dV
E���v0�X��F?�t3�Xh�wR(Ғ,5��4Ă��i0V�{)iJ��t�ԧ0�i�:nr{���d<���%r�A
Eڒ'CG���� `���"'�8��4��E�q���:���A���-R(���td��2'8@@�C��oVѽ׳�۫����g��ۋޟ�O��W�^�ެ�~,*	G 8�
�A�RI��A�f���*�9$���!�W傶��d��ܐm2�����t�ˬ�\+�iA�
�VI��/�r����djA��<~ �IQ
E&�I�4s�/�ka&� �~Y#�ٌg&��@�ŵn,��	/���<-@'�.��`Y�p �3�&�ט�0�ø�0�p(����0[�ٵp�0���][�I}���p(�0'���`^ ̤ �b�`���H3'.��a���L��������%�󴆦[�T�^00         [remap]

importer="texture"
type="StreamTexture"
path="res://.import/icon.png-487276ed1e3a0c39cad0279d744ee560.stex"
metadata={
"vram_texture": false
}

[deps]

source_file="res://icon.png"
dest_files=[ "res://.import/icon.png-487276ed1e3a0c39cad0279d744ee560.stex" ]

[params]

compress/mode=0
compress/lossy_quality=0.7
compress/hdr_mode=0
compress/bptc_ldr=0
compress/normal_map=0
flags/repeat=0
flags/filter=true
flags/mipmaps=false
flags/anisotropic=false
flags/srgb=2
process/fix_alpha_border=true
process/premult_alpha=false
process/HDR_as_SRGB=false
process/invert_color=false
process/normal_map_invert_y=false
stream=false
size_limit=0
detect_3d=true
svg/scale=1.0
              GDST~  �           T  WEBPRIFFH  WEBPVP8L;  /}�a �(j�HR��'�'���O�����%kZ	K�� �6�;F�/����Ћ 6w�V�$;i	���+;��(`�H���;���Ao�C��$M��T_>�<B�'�c++++�FFL}a9)R§682���A�i�S� � -�  ��ɔ����M��/zc���m&ֻ"b�'��@ǵ�
���2�Wh�
�;���v^�������ߝZ�׎w������˶ֶ��Y۶m۶m�cu:��v��y�g:�c��;�,�Y�vӶm[���r�ͭ۶�kDq:��k��۶m7�זR�>��m�ښ�·BSOQ�!>:�}w����Y�=�{�H�$7=���`�	vQ��U�E	!�}s;}g[��D��կ��݂d/��޷�m��wd03��1l�6����PIR�pq�|K �H�l[ �t:��������h�[U��t*3DX��dYZf&�a۶a����W3l�6�����H�$�\�ـ�J�?  �k�@�����O�n{�_ <\WA�H�eܯ���� �@�c �g���O��R�k@��	H�J� ��-yX#��O��q@�JB#��j\����~���S�cJHd�g=��=	��ڏ�n$����D`��ڏ�[��(1�X4$@}���6�=��&jd�g���(}����q ��& I	.���   B��H���j�)��<�z���͙��E�yʖ����ڱ �HRb�4M61U� ��b���ſ��+z����3�C�'�b� �j� �q�.V�E?Y���n�ë� ���;B�Y����JACզJ��\A��(���!�:��D/F��(�rG��y��ș� T/�#�T��}t�rG�z��OX��`���|�x�|�Z�7�j5?���Z�u�8�u1�l-M ��S�[��^���2�$vf[����k0��k�][�	��e�W� ��Y����,�l�N���Y~�b>�h�n�i�!;#��i�3�+;yT�^ڥ� ��^�.]�z��q6�>��lo�V�)_�mfcԵk5��\ߕ��в�r�f]J�vӣ  ��k*�<���;]+�C%��ێ�>s	���%�3�i}�g��Q5���8)������[ѱ �!��m��fZn	; �\�O�]��s�M��Yg9�M4��u�)�]�[�8GH��O�:��o�2���p
��]�`g}�)���O[������8q�1�c��s7-�А\egr`j�u�~�c{��\L��Rk)�Ӝ[��V�z�v����պm� �s�B��D�I��X�� �Z;� xXR�[s0�����b���g�Kߧ�U"���b�k\��   ��L���R�Ď�ǀ2����.2�r���ϲ�*���Ӵ�Ď�X`���� ��_�9�&0n�ǵ��"�x���]�LeMv�4o�~��r��};�\q|֊����k�<6��zZ ݋2}IT�u�
�r��ͱ\�xLA�u�cT>�r�.���n;���e�Vg'8�^o����� '\8���>4��9p��{y�gǯ����Ek'�Sr�Z����K�@���Uc�ש�ϭ�K6�
x���� �:��z͹ �c�ӣ�Vg�J��k ϊ{/j�W�
��� ? tnc�0{m�'O���ݷؐ�^�լb2l6i/I������\�e\}�5*�������K�7�BV�Y�_� � �ذ@ɥ����E <ھ e�Ns�8 ,:[  �
��	O�_����?m�Lud���c�is��@]K��W�ZZ����1 ,5=@L���]���0��+�#���}J�����K�v@�d�I�+vG�x�h�F��S� �tX��  T6k3{ٛ�� ��i�hs`�5H���[�_�K�z �" �dǍ��GF��RcyZΔ�b �b* ��@F�y�[��L@N @g��=�����M%F>��u�  _bd��gg*~���J�\ٻ�9 ���BOS4��T���x�Z��Z{U3� @lyPb����"����N�}�' �zS:-��r�ڗ�]�����]h7�W��NG�x  3��kn���G=%Bۮp�~< �9�� `(~��/ʳ2pk�|f�k5lf, �Z��ݚ��ե�tQ!s+^Jl���G^�������ƙ�k#��3��pQ�������?����{᜙9��f��X�އ��UL��=y�$&/��k37�+{uE���f��V��j�ڨ.0$��y/6�7t��7\ͽ( <.�Һm����v�}�r��E`I��Om�ː���?�{3c&]o��0ץ�xc���-\;��J�}�U����Qv���W |ۙ�W��`1 z��n��=��g�?V������ ��� "���:qv�|7 ��u�ڈ��1  X��}	 �3՚p05��   C}� a�Ry�.`�z[6�Qbmu�� x�2�zM?�.M��U�S�� ueʆ�P{c7�4M��b����@���er���,7r�����R�ܡv��@A���z���3��ݴ��W�b�C�����/�|^&���2�w�Ա����6lu�)a�� $*`� ���e"]�.��T~�]9�A^1 `�{q3O�r I�%��Ԃ����6�l'^�1�qL�#�}��Ā4���`p;�m�7
�����-  ��4�\�8��� ��,y��cp 4x#�G�ƒ%/������d��?��Db�(���\�xI��6���  ��-180,��(2l4��(�%R�-jD���Ȓ�  ��BCO]�w�G���^��P�N��&��p�M��~1�=�^��������  ��b HHC�df�Q �ь�1r�� ������zЋ �R�s,��!��\�
��H HF%��9U�/��H �� �R� ГM���/�J?��@  Z���Zr��#��DV� �`7� �_��k�~�  p�s P���- � xZ��,ܴ�eY\dV�!W�F+&3��^r! ���9 �H�   ���.`&�DӅ�LC"��3�64�@�B` @ps��0�K ϵ�� L�d�m�� *����[(u�Tɩ��=� �HQ(	��{ ��Py�� �.�   9f�E�[oߪo+m����i�g5#_r%s �R�
��e�A���vxvҕ�yL A\ ̄�Ju  {��9�W0MN".R>�O���R��O�O	����%a�� ��)zQ��L��{8��Ӗ 2�  ��2�e;�"ٷ��g���k~~��zf�� *��Л7 <��p�6  �'�t���''_[";j�<ξ,�%��|Q��F}�'y��S�&m��R�/��|Y�S��H��W���=�s�&��=  +�ZX� �
����S�z��'	Hq��gt�i	�I�	�B6\;!Ɇ�9�}�<����Ϳ �9n�$�C��' �ߕ��"���t�7/�2�Iȷi�;���3=>�'��o!�9��K3�P��/�S';�F6�;YK@�?s�(�Ǚ��~O
���(�gz8�	��p{r��e?��dy�p�p��YY��X���ǣ�#��y�bv�	�W�ƈ/�hx##��ŋU~��L��!1�!���5|�	�|0~T�l��6��� ���{��ŗt�q��xS�ɔ��S&6t�!�"�KZ��
D��P#M���X03�5��3'o�#U~�d&w��B�<eFZ�4����O$�K:��k��`(d��L�t�%4  ���*�K*KLF֜�`0ȓ6-.��R��F��G露�2�9,��<i|�_wC�E�[#�V-�D" �����Dq��o������Bcf�$��w�e�ɄD6s���2i��x���'�Y�F��A�WO�o����Z��/��	9�^O6�Y/�)�' ݟ��X�(�Ł���~�Z��y$�BR^��2o��@�����K��i��W�nR��ɯ)�y�_�7�=,�-�~<|G��|��2_י��<)/(�~����=8�	�;�􀄙϶��̡&����9��q⨖�)�9����\n��k����Ő�7r1�{�{m�J�|C�ۤ.d�����b�F�jK�=�ڲ+1dx�m�kd���|��0��j���^îl�$�ks�SU����s8�xs��Ԝ�x��ɘ�1S  �lo�l8�^��s�W��s܎d6�J��G���?�ˉ|�Ke�KV  b�Q˵cW��&�{}S��`_���<�  �|�E�ʅ��d�  ����bdڦ�2İ���U  ��H�|��d2Y ���x����?�7\'�x�ط���e^���Է.�I����o�l��2�8�#$���3���SK���	:n��Ĭd�Fr!�F9:���&�9����C-�&"���ep�F].j�Qkx��*���D25�Ω:� �ke����#X �18kx��sn�ⅆ'����x�S  lx0���)/����C:������~ё8,� $�^��n�4��:#%�kbp�C;_�K�2#'��6�7uzŸ��X� 98�n�_>��r��`���6OJ�t�Mͪ[Q}N�;.��Qk{�������˨�KV�疳��  p�y�ӳ�6�|�NM���^y�]N�*H�ܐep�m^[�v�+{�� �-����[�=�`q9��V���)�� ��ꔔ{�t|D�r��o�ߗj����*O��e��eb  �"=1��ʃ>������~}�O�\O��弳ۤZ�H��������}j�zH���nm9��T�#W�G4y(O0Ww���8O(oNas ��<K�����^@�SrFz=w���e6$8W����۱YdHD�����|���2TD��<|
��hñ8+3������8�ۙ|�-��  �c��cqxШNU��{���9,wxQ���o���졐��e?�werI-%���<$�^���-S����S�G�d�R)�/�eq�x���:v竌X�kxɰ�  ���ח��ŹT�Rgn  D�D2_����ψ�l[H��fF��<�=��I@T{.��Z��}ˡ7l1�!⯲]�Hr���͏��%�Yc.bp��Q-|	r5Io�o��,��L�E���W�c9j U$��C`�EH"%�mS������f��8S}z��x[ /�տ_�K�-$$�y$��[��YQ;;{���\4$|I�  A��<�2��֨<�梅{�f*B<����M���K%c/��&�	 ��c)ȋM���`P��詪���@"��x$}:]��lh��z������y.nn�� 7��Gɒa��Vl�t�@�[9b2x#ø��d��L�U�|ȰM���[��!ÆǏ:�=�d��V���u�����2�0xC|n��z{'���R���|pR/뵘,�n\�`3E2��܎ŃFx�Ր�d�v�ۙX��N����X�Z ��J.?�jn�{uS^���N��9,Y��u
���l�dnȟ�V}�AY22� �lh���U�LEyp{�N�׫�������|�-$�Q3#�~Mξ�����Wٮ281�ah����'B��܍D�����$RR�6կ�J��m���f�i�ә�T��I�����h�����H��s�޶SgII�L����`\*l!!��&�N�%s��]�}��X���/�  "h�!��j�D������In"��  ��ApY����ʈ
$���M����6{*Tܢ���[u�p�i� {��NK�h\*A�pNdo;{��ťk&�+96��P+}���cq�a�`S���!�8xh�[F�#���V���,��p�#�H���mo����z�\ /���s߸M��,y�Ⱥ�`ÃfC|�-$�Q3#�~M�/YE����!��Co�bTC�_e�*���*���`�����j�K�������_t���ر!24�n$R\-@z+$��ڶ�~UUj��y��/ƥ��Hp��Z  ��цD$8-ܓH5S����4�M�W  14.��SUSQ�D"�$�������?���Br>��Ҵ1C����������K���B`GQ3����o4x���ʏeq�ԥ�$Sk*��T��j��?^gŇ���p�� ���,`o�<������Ο� g�>���Q#o�Y��ܤ����LN)�����A����Civ۳'�P-�.,N��+U�U�/�s@;6��Sr�Lm�p|�	����l[�6�p�^�zz^��.���BŖ����H�^�Y  d�0�fqN�'�k5u��✒�=�M��ׂ����t�����`|��O������k�i
�������o  ��x����Rc�@ � [remap]

importer="texture"
type="StreamTexture"
path="res://.import/play1_screen.png-459948682aa57fadb7195ffa863c0b1a.stex"
metadata={
"vram_texture": false
}

[deps]

source_file="res://images/readme/play1_screen.png"
dest_files=[ "res://.import/play1_screen.png-459948682aa57fadb7195ffa863c0b1a.stex" ]

[params]

compress/mode=0
compress/lossy_quality=0.7
compress/hdr_mode=0
compress/bptc_ldr=0
compress/normal_map=0
flags/repeat=0
flags/filter=true
flags/mipmaps=false
flags/anisotropic=false
flags/srgb=2
process/fix_alpha_border=true
process/premult_alpha=false
process/HDR_as_SRGB=false
process/invert_color=false
process/normal_map_invert_y=false
stream=false
size_limit=0
detect_3d=true
svg/scale=1.0
        GDST�  �           �  WEBPRIFF�  WEBPVP8L�  /��a �(h�F:w�Q�~1���L�j����{}r�p��	��n 9 ��������c��4�ܕ��i���>��I��,M�m۶�f��?	����='��
�d��m�i�]���<x������m#G�^�bu�H���ز?�a���5��'۶m۶�g۶m����� ��Wn#I����ݽ��KdL�14�@�9��$)R��*�/��F���ߑI��8��@�mkk�t�033'f���Y��D0���s��:�������R��жm��.��۶m��m۶m�m����V��[�mm�$}"2#�b�Զm���n`��Cp�F�D��3�Aw��/�-I�i[56�m۶m۶m�Ƶul�Ws.\�}	(��"�	����F�|̋�/�R@��D�,!0O  > ��� )+��d��{������L�5��,@�Ht=;����x��K] qm >�.�H_4����<:0�Pd��3bӪ:u����E�_�`Qj@B:g qdY��[�� � �	�CE���끶1^���o���_������o�� }=�>�ر @[�د& a|_D3)�U�������3���1r��XId!1r.Sшk�	;V��Jj1��Fe�JSOh�M��ɞ�Ag�����Ik#��RO1��VI$&�T�$2�<Y`�f�cw��=������B�t�y�OuO�*T��SCx�FL���~cj��;��bh<-�v�us<- бb��C��~|Jܤ�&@bgI:S3&�i܏ P�q?f<U�M:cgY:�lO�p���V�r�4��V��Ԉ����n��X�{������;W�n��;�d6��D�p��0vt�o�Q$ns0&���쇎W�c��9��M� ���ܘ��!��V ���i��w/p�Ӱlz���]$�L�3�4���x��9���)T:��SO�  ���d��@)K�kÒ��n ���y&��!�2d��:����G�~Ğ��Y�,D ���s��>q�i׫��5�%P�o�в?5�("X����O������mv�$��n�y1;FRt ^��#4����'�'B�ȵ�O,) ��0��� j|��U��S�#��§΅94��/�Cs�^1$0q��L��V�:Y��=�v�z�~��=��r��H@�؎��q<HP�N�6Z�-
�y'���`ߝ'���̿o��Oݑ�[��F%&���N=n�"t��J�N}���P_�hR�e̗1���Rg���g �~��� ��m/�	OE�K"�s���P���P]z&�������F@��,X�t�n�iO��mr��l�F���=6��gA�ӑ�����bxE��R��ָ4I�8bP�IC7���4�{"Ġ�h �9�o����%��߷����f���8�^|�l�N�&��e�T�ωq�˅�l;_R����y$��/�=o퇃1��~?�иuT���!�$?�Arkbv@:��s"�8��y��!a�!� '2�o�T�������-�GϷ��-��/������+9^��D؈8���Ġ��@�~켖�rJV��\���@d٦X��o��:�Ʋ�J۶}������o���v o+�T?'=���u����W�#����O}W0Ѝ4��c�9�� �h\�eʖ)h"]��c��5�J��ĶGX��;y	�E�_'q��*ru-���[�bçb�|q�����)��kG��w���hv��x MzN"�X�� �"+�a�������ܓ�`?����&@�m:ϯ�8�n�"�L��4|M���e�<�n�ݘ���&ŋ�#�I _H�+�Ԫ�\�~N9�V=5%+1۸��еó]���R��Y&��E�(W&��ԍDm����~3�[��d�鷚\����#n'������uA��o؊��=}7�J346��\�^��m��>m�h���a|i�\�bi��Ӝpc��}H�\���pcV
D�z`���[�� &nF�Fnr���?(��My-�� �y��I�����0$�d�4�J��=$1��쇚�I�����p�mD����sa�Ʌy)�;�Zb����p�Di�%j�@r �9WD*���+��q��*%~c��o�y�H��k�ק��րu��F��]��g���-�M�1U��ͷ]ݞ�����l$��˟��{1�v"���ē�U`���aj�뙢K]��NM�a��f��.��a6(�#�Bx�&v5�z�n��Zj�Q�1��q5�+� k ��}ͤVi^�RX4��J[���dد2�
�VV H`�� �EF��� H�Hi@9@�d!�e�ߜ�a���
�
�D�.�Eu6�07	���B�)� �a�o�rX�n&�d�*Ѽ�� ��a�N�+�p� By�aX:�K�d@u�"�L3���A������P�x	�9�@*y,�$�`��+:!�x�v�)�|���ufY��p�B��
5���`�� ��I@`�U��x�|�HD��|�Z��)`ME�� މr��3N)d�F,�����1먊o�j�ď�\����� �5 �|��r�(ş��^��>"�$�ό�O�x�,S\�ʖ��d�P"��b�Q8|g��t���e&Y�#؁A2�&�-Nf���=z��/��sRJ��v]mIE4l�s	X��T�`>���6�k+
��6�b`B5���xj������+`;'���.��\A{ӻ��R	��2=���y��ZP2��a2S�.��o�+�V�K\-y:������gW��!����N����Z6�z����|D�~ӻ�s���v���ǡ�VlN�t�s�����#*�2��'d!�5�8�)��x����H�2�-��-+�UB5����ec��10髀��v- w�\�6�т�ͻ'���7�,+�F�Wv����S��ɔXTS��1��9�m�B���?��OZ�p������tf�M�b�9��b��h��#���m��P�����;��w���Y9~�sz�y�X���\�YG��4B`B5��D�ڲ�8|�� ��v�
�j����rv�#����C�q8|2�jIݿЂJ`���h��Q�Pݿ�99��� �ha۲���_���"w�]�<�4��%��L�X�ˈ_s�m��ec�E#����!����������XK?R�"�{C�H��y�CN[Q.��x���{j�Б��&��d����^��-�������}���=�P�#1�qud�l|6��~�l�e>��q����+�o.)�V+O.g~<S��l����qT�}�r�w�Lόh�F��1r>�N#�O-4�s-��l�6�N���>�'­�c�7��H�W#����>�g�F�Bf���9���ms��,�G��+�}�������{{��ѷ/��\)��������`�5ӎ���C����c���x��=��X~q�ha���^E�C�9���bJ%i�����E`��ޫС<���Y#g��4-݈�l���r�o7��ILG�7|�R��T&�ސɚv1r�JL�1R���|.�<�xD��9r���#�8J���ٍ��=`���E�S�_Gַ-�ec�ya�`Jy�����c�����?������c�����?Y2"l��� 9im�a�gth&���e��V�]���]�d�4��wh`��1Rj�fc��Xiup���+t}]�罬��j�㤳���;����|n]�F�f��q$�������\��rrY����`��cl�Ϯtb�z���#(����\)��:
j��Gl�+�0����LL��e{E;i�G���?������c�����?������c���������S����@~ M�#�&��i�����l0m0��G�@��]�遀��?%@*P�@��
`
��3��  [remap]

importer="texture"
type="StreamTexture"
path="res://.import/play_screen.png-f36ca6b9c6993300740fad28a0118d78.stex"
metadata={
"vram_texture": false
}

[deps]

source_file="res://images/readme/play_screen.png"
dest_files=[ "res://.import/play_screen.png-f36ca6b9c6993300740fad28a0118d78.stex" ]

[params]

compress/mode=0
compress/lossy_quality=0.7
compress/hdr_mode=0
compress/bptc_ldr=0
compress/normal_map=0
flags/repeat=0
flags/filter=true
flags/mipmaps=false
flags/anisotropic=false
flags/srgb=2
process/fix_alpha_border=true
process/premult_alpha=false
process/HDR_as_SRGB=false
process/invert_color=false
process/normal_map_invert_y=false
stream=false
size_limit=0
detect_3d=true
svg/scale=1.0
           [remap]

path="res://Scenes/Scripts/Ball.gdc"
  [remap]

path="res://Scenes/Scripts/Paddle.gdc"
[remap]

path="res://Scenes/Scripts/World.gdc"
 �PNG

   IHDR   @   @   �iq�  qIDATx��{pTU����~���I�A	$2$�H E, 
(�>��ؙ\vvqtwj�ف�}��H�S�̨�*����0��
��S^�
B!$��t������!��N��t�_U�n�9�;������sסO�'}��QA!~'i�E�Dw2��P��w=�%EY}���c�˕�ɢ�!u�$m�)��qz�ȷD�~F����y��ly���n\s�i�}�����$!�Pi��4:$YF�V��g%H0��u��� *�.����zr�W֥Ô���V-���>��Z�G�d�����B I2*��FK��A� !��_�#1��'��:� �MH��V�ܯ~\mV���-�HZYZ(@Vk���H�
�V�@�ZYZ(Ձd��V����akPxP�������:[�$�����KJ00u|	�Y�^��43u|	I	�7��U ��������=e<��*bX�@�(�¦���C��ŢG�[6!AE���%v)g���X[�EϘ;�hh�L徱E!Q^�ʻ{�9|��qy&�*�����OEu]�r��Y�-�Ⱥ�8x����F�2����,���u��n�ɩs�1�7�H���w�/���/��x=��3���ɜ�y����ov)���������%\� ��6��%-A�̒L�����y<��JZ�Θ��i޴��HN�g�;�x{Ou��� �r���;�L�@3�Ņ�3f�������<����%�"91�iG�R�ض ����8�L��2��cu̽+��/[��}��˖�u!�tȼ��XS�]k�	:5�������3��t��ڝ��W������(�]�7uĊ�:��� �
��֨ew��#VD��j���L�NO��Z��CfG���N}�Q��_>�ߟ6�݇��ݻ��t�
z���/z��&�Mu���F[Ԋ�Fe���&;���S����͍�<^����9L�= ��c+���\1bړ
���6��?������_�������%���wSZ\@^N&�y���*7oiT�`^� ����z�<���'��{�/`Ъ��������x��;��r4[�eFR��b�̛�ί/�v� v�n�G�����qx�,�t��5�}2�7�A�u�/p���{x�e�Y�e��9�9�'�X2o&��>?p�W��� �dֿ�sj��<���@ׯ�o���	P���yё��Ǡ��l�wy���}l����l�WZB��h�v�˗�C�լ�v��� ���o�x��W7�|�U�N#�j�/�׫���=��qŅ���*�B��#�mn��k��=�4lް���E���'b�����\�:��6_X44Y�6�1� ��{;3� �lSM�']a��ǻ)��a�LML�OLK�P^YÇ��7��� ���:Ԓ�s�T���QK
���:�����(p��
����ݹ)��ҺՔ��;7�W�n�bÕ^�G4�ӧ{Y��2��=�Ѫ$��CI��AC����c���z����df��dHZ�/�k���`m�+ǁ�8�>?�toD�#^�u���8/�E�1 ��ƽ��ܓ�ʻ{����ͅ�����\`����J�(+�������hvz{(�T<��`Oq�ed�8w��-f��G���l6n�Ȃ���HY.=QGzb�����,�������l��~�ŋs��9��q,-+�F��sH,^�b�n7�~����� 8�N^�u����������dV/,e��RJ����Oaf߾�����8��A����,��r�n7����{8���b@AEE��[��]�< ���|s�$�$A�9�������}�X,TTT �`pJ���쀦k�O�V2]���d�~�ds�ф�{t��d�{|��hlss���|

��.%Ib����>v���̙3���*����ū���Ob�
+����2����b�op���<��̝�q�ͬX����HO7S�@+W�@A��'�P=6A�3m����xfZ���!pz�l=ZǺ��!��=�������yj�`^x�e�4*E��e�>��*K������p���O��
�Z����9|�`�ח1^[_�8u9d_@a͞j��&˨�j���B�Yb�7��z5�PX�eM�r#�y3��'�b@�-@����Q�I�Ë�����\�쥢������e$�(�H�d�`2h04�����j����r����7ٹb�`4h�I�c� c�<-.~�ցee%1:��c:���fN��|`ţ%dܰ(�j�`ux9��ƀ-9ƞm7Up��*�����x��O�?��Ia�?�@٦���RY��P������_Tauxivt]��=��؋%��|� *��r���uV%�Ft*	w���,	2���l�;+�����|J�R�%A ��N%��%ꬮ�A�,��C����lms`��9,f���
���7�ʜ�C%K�\qt�7��\q��%�7��a����.u��h���j�-��BÇ���n��7�UO|s�������iq�P�}к0{i�?4! 9N�A�"N�U͎�B1<#���z�������)&��=�2�8U߽����2No��x�
�V�ɠ!��s�^�U���K=wp�Z�:<��khs�hluc���:�8<~?W�^v�m
Y�(+�Lck���Ԭ�7G�5����3�s�v~��I���ǐ�<�����~(�xk���ǋk61,#��(�v:G-D{$xXF"/��ԫ���0�c�1r�SY2o&*Y���g�m+<RT��_�c�p3�}����EW>�
Wo����ҧf�o��՝���[��f���O��m2���-�����eȩy#�@��<�t���5L9���C�����r?O�����lb��!�pR�--,e-�v�J���>TF�	�B�R�Y�3����i����t#'j[8Uߊ�~W&Rtj���$�s��2�Lj���6;�?���>��?�%rG'�'t��e&�UĬ)�_2�,*TYlT_qP�좱͍��M�ˇ� Th�vL.I�F�z�L�^MZ�s��,���IK@��@�/��a�_��H9���;8���VMqa.%����#7�L�)��'���T�5r��y�WTs�L5�(z�pDuF(�O�������4j�&2R�$���kQIƤ�p���N ����fw���LC��/�9�͢RP��(J�������[���op+P�����B=�_&���i�B��t���w����a��k�.	X�(J��q}'ZA0���q�(J@�j���5jԨUw|d�!�m�J�?n{(
N����s�o�~�]��S}|�dP�=.%�B�;��h��Y?>����Bvۮ��o=,�i]��IZ�w��˳��lY��u���NCWj^��.%e���O-T&�(:��s������|�3�y?��c�ٓ��Z}�s�����J~u�qK    IEND�B`�      ECFG      application/config/name         Breakout   application/run/main_scene$         res://Scenes/LevelOne.tscn     application/config/icon         res://icon.png     display/window/size/width      �     display/window/size/height      h  )   rendering/environment/default_environment          res://default_env.tres          