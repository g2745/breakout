# Breakout [2018]

Created basics of a breakout game [Followed Tutorial].

## Playable Example
[Play Breakout](https://majorvitec.itch.io/breakout)

## Game Overview

### Playing Screen

![One Ball Playing Screen](/images/readme/play_screen.png "One Ball Playing Screen")

![Multiple Balls Playing Screen](/images/readme/play1_screen.png "Multiple Balls Playing Screen")
